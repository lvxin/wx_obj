<?php
/**
 * 用户管理
 * User: Administrator
 * Date: 14-10-27
 * Time: 下午5:05
 */

namespace Home\Controller;


class UserController extends BaseController{
    //用户管理首页
    public function index(){
        $subscribe = I("subscribe");
        if(!empty($subscribe)) {
            $map['subscribe'] = 1;
        }
        $data = D("User")->where($map)->findPage(10);
        $this->assign($data);
        $this->display();
    }
    //同步openid
    public function pullUserList(){
        D("User")->pullOpenid();
        //TODO 异常待处理 add by lvxin
        $this->success("同步成功");
    }
    //用户信息
    public function pullUserInfo(){
        $openid = I("openid");
        if(empty($openid)){
            $this->error("openid不能为空");
        }
        $user = D("User")->pullWeixinUserInfo($openid);
        if(empty($user)){//同步失败
            $this->error("同步失败");
        }else{//同步成功
            $this->success("同步成功");
        }
    }

} 