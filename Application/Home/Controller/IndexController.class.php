<?php
namespace Home\Controller;

class IndexController extends BaseController {
    //推送系统首页
    public function index(){
        $localUserCount = D("User")->getLocalUserCount();
//        $localGroupCount = D("Group")->getLocalGroupCount();
        $this->assign("_localUserCount",$localUserCount);
        $this->display();
    }
}