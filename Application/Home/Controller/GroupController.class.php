<?php
/**
 * 分组管理
 * User: Administrator
 * Date: 14-10-27
 * Time: 下午5:06
 */

namespace Home\Controller;


class GroupController extends BaseController{
    //分组管理首页
    public function index(){
        $data = D("Group")->where("is_del = 0")->findPage(10);
        $this->assign($data);
        $this->display();
    }
    //同步分组
    public function pullGroupList(){
        $res = D("Group")->pullGroupList();
        if(empty($res)){
            $this->error("同步失败");
        }else{
            $this->success("同步成功");
        }
    }
} 