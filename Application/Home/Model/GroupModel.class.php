<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 14-10-27
 * Time: 下午7:29
 */

namespace Home\Model;


class GroupModel extends BaseModel{

    /**
     * 同步组列表
     * @return boolean
    */
    public function pullGroupList(){
        $res = self::$wechatObj->getGroup();
        if(empty($res)){//接口调用失败
            return false;
        }
        //获取本地分组信息
        $localGroupList = $this->index("id")->select();
        $weixinGroupList = $res['groups'];
        foreach($weixinGroupList as $wk => $wv){
            if(empty($localGroupList[$wv['openid']])){//本地不存在，添加分组
                $this->add($wv);
            }else{//本地存在分组，更新分组
                $this->where("id = {$wv['id']}")->save($wv);
            }
        }
        return true;
    }
} 