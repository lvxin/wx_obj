<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 14-10-24
 * Time: 下午2:27
 */

namespace Home\Model;


use Think\Model;

class BaseModel extends Model{

    static protected $wechatObj;//微信类对象

    public function __construct(){
        parent::__construct();
        //引入微信类文件
        vendor("Wechat",VENDOR_PATH . "Weixin",".class.php");
        //读取微信配置
        $option = C("WEIXIN_OPTIONS");
        //创建对象
        self::$wechatObj = new \Wechat($option);
    }
} 