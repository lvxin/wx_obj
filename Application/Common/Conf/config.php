<?php
return array(
    //'配置项'=>'配置值'
    "WEIXIN_OPTIONS" => array(
        'token' => 'token', //填写你设定的key
//        'encodingaeskey' => '', //填写加密用的EncodingAESKey
        'appid' => 'wx5833f91716212500', //填写高级调用功能的app id
        'appsecret' => '1166bda08c95be55eac1d3d1fe482edb', //填写高级调用功能的密钥
//        'partnerid' => '88888888', //财付通商户身份标识
//        'partnerkey' => '', //财付通商户权限密钥Key
//        'paysignkey' => '' //商户签名密钥Key
    ),
    /* 数据库设置 */
    'DB_TYPE'               =>  'mysqli',     // 数据库类型
    'DB_HOST'               =>  'localhost', // 服务器地址
    'DB_NAME'               =>  'weixin_push',          // 数据库名
    'DB_USER'               =>  'root',      // 用户名
    'DB_PWD'                =>  'root',          // 密码
    'DB_PORT'               =>  '3306',        // 端口
    'DB_PREFIX'             =>  'push_',    // 数据库表前缀
);