<?php
/**
 * 自定义函数
 * User: lvxin
 * Date: 14-10-28
 * Time: 上午8:44
 */
//拆分数组
function getSubByKey($pArray, $pKey="", $pCondition=""){
    $result = array();
    foreach($pArray as $temp_array){
        if(is_object($temp_array)){
            $temp_array = (array) $temp_array;
        }
        if((""!=$pCondition && $temp_array[$pCondition[0]]==$pCondition[1]) || ""==$pCondition) {
            $result[] = (""==$pKey) ? $temp_array : isset($temp_array[$pKey]) ? $temp_array[$pKey] : "";
        }
    }
    return $result;
}